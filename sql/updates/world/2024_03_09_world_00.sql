-- Kurong Caskhead
DELETE FROM `npc_vendor` WHERE `entry`=64047;
INSERT INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES
(64047, 0, 81406, 0, 0, 0, 1),
(64047, 1, 81414, 0, 0, 0, 1),
(64047, 2, 74636, 0, 0, 0, 1),
(64047, 3, 81407, 0, 0, 0, 1),
(64047, 4, 81415, 0, 0, 0, 1);
